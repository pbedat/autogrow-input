import { useMemo, useRef, useState, useEffect } from "react";
import autogrow, { AutogrowOptions } from "./autogrow";

function useAutogrow(
  inputRef: React.MutableRefObject<HTMLInputElement | null>,
  options: AutogrowOptions = {}
) {
  const ctrl = useRef<ReturnType<typeof autogrow>>();

  useEffect(() => {
    if (!inputRef.current) {
      return;
    }

    ctrl.current = autogrow(inputRef.current, options);

    return () => ctrl.current?.destroy();
  }, [inputRef.current]);

  useEffect(() => {
    ctrl.current?.update(options);
  }, [options]);

  return ctrl.current;
}

export default useAutogrow;
