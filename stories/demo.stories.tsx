import React, { useRef, useState } from "react";
import { storiesOf } from "@storybook/react";
import { withKnobs, text, number, boolean } from "@storybook/addon-knobs";
import useAutogrow from "../src/use-autogrow";

const Demo: React.FC<{
  placeholder: string;
  style: React.CSSProperties;
  snug: boolean;
}> = (props) => {
  const [placeholder, setPlaceholder] = useState("foo");
  const [spacing, setSpacing] = useState(0);

  const inputRef = useRef<HTMLInputElement | null>(null);

  useAutogrow(inputRef, { snug: props.snug });

  return (
    <input
      ref={inputRef}
      placeholder={props.placeholder}
      style={{
        letterSpacing: spacing,
        padding: ".25rem .5rem",
        border: "2px solid black",
        ...props.style,
      }}
    />
  );
};

storiesOf("demo", module)
  .addDecorator(withKnobs)
  .add("Hook", () => (
    <Demo
      snug={boolean("snug", false)}
      placeholder={text("placeholder", "woop")}
      style={{
        fontSize: number("fontSize", 14),
        letterSpacing: number("letterSpacing", 1),
      }}
    />
  ));
